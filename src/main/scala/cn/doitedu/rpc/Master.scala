package cn.doitedu.rpc

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

import scala.collection.mutable
import scala.concurrent.duration.DurationInt

class Master extends Actor {

  val idToWorker = new mutable.HashMap[String, WorkerInfo]()


  override def preStart(): Unit = {
    //启动定时器，检测是否有超时的Worker
    import context.dispatcher
    context.system.scheduler.schedule(0 seconds, 15 seconds, self, CheckTimeOutWorker)
  }

  //接收消息的方法
  override def receive: Receive = {

    case RegisterWorker(workerId, memory, cores) => {
      //将接受的参数封装起来，然后保存
      val workerInfo = new WorkerInfo(workerId, memory, cores)
      //idToWorker.put(workerId, workerInfo)
      idToWorker(workerId) = workerInfo;
      //idToWorker.+=((workerId, workerInfo))
      //向Worker返回注册成功的消息
      sender() ! RegisteredWorker
    }

    case Heartbeat(workerId) => {

      //根据发生过来的workerId，找出对应的WorkerInfo
      val workerInfo = idToWorker(workerId)
      //更新对应WorkerInfo的最近一次心跳时间
      workerInfo.lastHeartbeatTime = System.currentTimeMillis()
    }

    case CheckTimeOutWorker => {
      //检测超时的Worker
      val deadWorkers = idToWorker.values.filter(w => System.currentTimeMillis() - w.lastHeartbeatTime > 10000)
      deadWorkers.foreach(w => {
        idToWorker -= w.workerId
      })
      println(s"当前活着的Worker数量：${idToWorker.size}")
    }

  }
}

object Master {

  val MASTER_ACTOR_SYSTEM_NAME = "MasterActorSystem"
  val MASTER_ACTOR_NAME = "MasterActor"


  def main(args: Array[String]): Unit = {

    val masterHost = args(0)
    val masterPort = args(1)

    val configStr =
      s"""
        |akka.actor.provider = "akka.remote.RemoteActorRefProvider"
        |akka.remote.netty.tcp.hostname = $masterHost
        |akka.remote.netty.tcp.port = $masterPort
        |""".stripMargin
    val config = ConfigFactory.parseString(configStr);
    //1.创建ActorSystem(单例的)
    val masterActorSystem = ActorSystem.apply(MASTER_ACTOR_SYSTEM_NAME, config)

    //2.使用ActorSystem创建Actor
    masterActorSystem.actorOf(Props[Master], MASTER_ACTOR_NAME)


  }

}
