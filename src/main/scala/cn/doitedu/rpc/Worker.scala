package cn.doitedu.rpc

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

import java.util.UUID
import scala.concurrent.duration.DurationInt


class Worker(val masterHost: String, val masterPort: Int, var memory: Int, var cores: Int) extends Actor {


  val WORKER_ID = UUID.randomUUID().toString

  //生命周期方法：一定被调用，并且按照一定顺序调用
  //在构造方法之后，receive方法执行之前，执行一次
  override def preStart(): Unit = {
    //Worker向Master建立连接，并发送消息
    val masterRef = context.actorSelection(s"akka.tcp://${Master.MASTER_ACTOR_SYSTEM_NAME}@$masterHost:$masterPort/user/${Master.MASTER_ACTOR_NAME}")
    //Worker向Master发送注册消息
    masterRef ! RegisterWorker(WORKER_ID, memory, cores)
  }

  override def receive: Receive = {

    case RegisteredWorker => {
      //1.启动一个定时器，定期向Master发送心跳
      import context.dispatcher
      context.system.scheduler.schedule(0.seconds, 10 seconds, sender(), Heartbeat(WORKER_ID))
    }

  }
}

object Worker {

  val WORKER_ACTOR_SYSTEM_NAME = "WorkerActorSystem"
  val WORKER_ACTOR_NAME = "WorkerActor"

  def main(args: Array[String]): Unit = {

    val masterHost = args(0)
    val masterPort = args(1).toInt
    val workerHost = args(2)
    val workerPort = args(3).toInt
    val workerMemory = args(4).toInt
    val workerCores = args(5).toInt

    //1.创建ActorSystem（单例）
    val configStr =
      s"""
        |akka.actor.provider = "akka.remote.RemoteActorRefProvider"
        |akka.remote.netty.tcp.hostname = $workerHost
        |akka.remote.netty.tcp.port = $workerPort
        |""".stripMargin
    val config = ConfigFactory.parseString(configStr);
    //1.创建ActorSystem(单例的)
    val workerActorSystem = ActorSystem.apply(WORKER_ACTOR_SYSTEM_NAME, config)

    //2.创建Actor
    workerActorSystem.actorOf(Props(new Worker(masterHost, masterPort, workerMemory, workerCores)), WORKER_ACTOR_NAME)


  }

}
