package cn.doitedu.rpc

case class RegisterWorker(workerId: String, memory: Int, cores: Int)
